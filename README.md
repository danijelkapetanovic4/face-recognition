# How to Generate Makefile

To generate a Makefile, follow these steps:

1. Open a terminal.
2. Use a text editor like nano to create a Makefile.
3. Enter the following code into the Makefile:

```makefile
# Compiler
CC = g++

# Compiler flags
CFLAGS = -std=c++11 -Wall

# OpenCV flags
OPENCV_FLAGS = pkg-config --cflags --libs opencv4

# SQLite3 flags
SQLITE3_FLAGS = -lsqlite3

# Source files
SRCS = your_source_file.cpp

# Executable name
EXE = your_executable_name

all: $(EXE)

$(EXE): $(SRCS)
    $(CC) $(CFLAGS) $(SRCS) -o $(EXE) $(OPENCV_FLAGS) $(SQLITE3_FLAGS)

clean:
    rm -f $(EXE)
```

4. Save the file as Makefile in the same directory as your source code.
5. Replace your_source_file.cpp with the name of your source file and your_executable_name with the desired name of your executable.
6. Navigate to the directory containing your files in the terminal.
7. Run make to compile your code.
8. If compilation is successful, execute your program by typing ./your_executable_name.
9. To remove the generated executable file, use make clean.
