#include <opencv2/opencv.hpp>
#include <sqlite3.h>
#include <string>
#include <vector>
#include <map>

using namespace std;
using namespace cv;

void serializeMat(const cv::Mat& matrix, vector<uchar>& buffer);
void deserializeMat(const vector<uchar>& buffer, cv::Mat& matrix);

class DBHandler
{
    public:
    sqlite3* db;

    DBHandler(bool *var,  map<int, string>& all_users, vector<cv::Mat>& all_faces, vector<int>& all_labels);

    void addUser(int key, std::string name, std::vector<cv::Mat> faces,    
                    std::vector<int> labels);

    void deleteUser(int key);
};

