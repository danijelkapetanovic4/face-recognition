#include <opencv2/opencv.hpp>
#include <opencv2/face.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <sqlite3.h>

#include "DBHandler.hpp"

using namespace cv;
using namespace std;
using namespace cv::face;

bool model_update_status = false;
bool trained = false;

CascadeClassifier face_cascade;
Ptr<LBPHFaceRecognizer> model;

vector<Mat> all_faces;
vector<int> all_labels;
map<int,string> all_users;

// initializing database
DBHandler database(&model_update_status, all_users, all_faces, all_labels);

void init()
{
    model = LBPHFaceRecognizer::create();

    // Load pre-trained face detection model
    if (!face_cascade.load("haarcascade_frontalface_default.xml")) {
        cerr << "Error: Couldn't load face detection model." << endl;
        exit(21);
    }
    
}

int isInteger(std::istream &cin)
{
    if (std::cin.fail()) {
         std::cin.clear();
        // Ignore any remaining characters in the input buffer
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        return 0;
    } else {
        return 1;
    }
}

int readInput(int* input_value)
{   
    cin >> *input_value;
    while(!isInteger(cin))
    {
        cout << "> Invalid input value!" << endl;
        cout << ">> ";
        cin >> *input_value;
    }
    return *input_value;
}

int  checkIfUserExist(vector<Mat> faces)
{   
    if (trained)
    {
    
        // Perform face recognition on each captured face
        vector<int> predictedLabels; // Store all predicted labels
        for (size_t i = 0; i < faces.size(); ++i) 
        {
            int predictedLabel = -1;
            double confidence = 0.0;
            model->predict(faces[i], predictedLabel, confidence); // Predict label for each face
            //cout << "predictedLabel: " << predictedLabel << " confidence: " << confidence << endl;
            if (confidence < 30)
                predictedLabels.push_back(predictedLabel); // Store the predicted label
        }

        // Count the occurrences of each predicted label
        map<int, int> labelCounts; // Key: predicted label, Value: count
        for (int label : predictedLabels) {
            labelCounts[label]++;
        }

        // Find the label with the maximum count
        int maxCount = 0;
        for (const auto& pair : labelCounts) {
            if (pair.second > maxCount) {
                maxCount = pair.second;
            }
        }

        // Check if the final predicted label has enough votes
        if (maxCount >= 10) {
            cout << "ERROR: This user is already registered! (Votes: " << maxCount << ")" << endl;
            return 1;
        } 
        return 0;
    }

    return 0;
}

bool checkKey(int key)
{
    auto it = all_users.find(key);
    if (it != all_users.end()) {
        std::cout << "ID " << key << " exists in the map. The corresponding value is: " << it->second << std::endl;
        return false;
    } 
    return true;
}



void addNewUser()
{
    string name;
    int key;

    cout << "> Enter key: " << endl;
    readInput(&key);
    while (!checkKey(key))
    {
        cout << "> Enter another key: " << endl;
        readInput(&key);
    }
    
    cout << "> Enter name: " << endl;
    cin >> name;

    // Create vectors to store face images and corresponding labels
    vector<Mat> faces;
    vector<int> labels;

    // Open the camera
    VideoCapture cap(0);
    if (!cap.isOpened()) 
    {
        cerr << "> Error: Couldn't access the camera." << endl;
        return;
    }

    // Main loop for capturing faces
    for (int i = 0; i < 25; i++)
    {
        Mat frame;
        cap >> frame; // Capture frame from the camera

        Mat gray;
        cvtColor(frame, gray, COLOR_BGR2GRAY); // Convert frame to grayscale

        // Detect faces in the frame
        vector<Rect> faces_rect;
        face_cascade.detectMultiScale(gray, faces_rect, 1.3, 5);

        // Create a named window with a specific size
        namedWindow("Face Recognition", WINDOW_NORMAL); // WINDOW_NORMAL allows window resizing
        resizeWindow("Face Recognition", 800, 600); // Set the desired width and height



        // Process each detected face
        for (Rect face_rect : faces_rect) 
        {
            Mat face = gray(face_rect); // Extract the face region

            // Draw rectangle around the face
            rectangle(frame, face_rect, Scalar(255, 0, 0), 2);

            // Add face data to the containers
            faces.push_back(face);
            labels.push_back(key); // Label for the face

            // Display the frame
            imshow("Face Recognition", frame);
            waitKey(1);
        }
    }
    printf("- Number of samples: %ld\n", faces.size());

    // Release the camera
    cap.release();
    destroyAllWindows();
        
    // Check if user already exist
    //if(true)
    if(!checkIfUserExist(faces)) 
    {
        for (auto face : faces)
        {
            all_faces.push_back(face);
        }
        for (auto key : labels)
        {
            all_labels.push_back(key);
        }
        model_update_status = true;

        cout << "> User successfully added to training set!" << endl;

        database.addUser(key, name, faces, labels);

        cout << "> User successfully added to DATABASE!" << endl;

        all_users.insert(make_pair(key,name));

    }

}


int verifyUser()
{
    if (!trained)
    {
        cout << "> ERROR: There are no users in system.." << endl;
        return -1;
    }

    printf("> Scanning user...\n");

    // Create vectors to store face images and corresponding labels
    vector<Mat> faces;

    // Open the camera
    VideoCapture cap(0);
    if (!cap.isOpened()) 
    {
        cerr << "Error: Couldn't access the camera." << endl;
        return -1;
    
    }
    namedWindow("Face Recognition", WINDOW_NORMAL); // WINDOW_NORMAL allows window resizing
    resizeWindow("Face Recognition", 800, 600); // Set the desired width and height

    // Main loop for capturing faces
    for (int i = 0; i < 20; i++)
    {
        Mat frame;
        cap >> frame; // Capture frame from the camera

        Mat gray;
        cvtColor(frame, gray, COLOR_BGR2GRAY); // Convert frame to grayscale

        // Detect faces in the frame
        vector<Rect> faces_rect;
        face_cascade.detectMultiScale(gray, faces_rect, 1.3, 5);

        // Process each detected face
        for (Rect face_rect : faces_rect) {
            Mat face = gray(face_rect); // Extract the face region

            // Draw rectangle around the face
            rectangle(frame, face_rect, Scalar(255, 0, 0), 2);

            // Add face data to the containers
            faces.push_back(face);
        }

        // Display the frame
        imshow("Face Recognition", frame);
        waitKey(1);
    }

    printf("-Number samples: %ld\n", faces.size());

    // Release the camera
    cap.release();
    destroyAllWindows();

    // Perform face recognition on each captured face
    vector<int> predictedLabels; // Store all predicted labels
    for (size_t i = 0; i < faces.size(); ++i) 
    {
        int predictedLabel = -1;
        double confidence = 0.0;
        model->predict(faces[i], predictedLabel, confidence); // Predict label for each face
        //cout << "predictedLabel: " << predictedLabel << " confidence: " << confidence << endl;
        predictedLabels.push_back(predictedLabel); // Store the predicted label
    }

    // Count the occurrences of each predicted label
    map<int, int> labelCounts; // Key: predicted label, Value: count
    for (int label : predictedLabels) {
        labelCounts[label]++;
    }

    // Find the label with the maximum count
    int maxCount = 0;
    int finalPredictedLabel = -1;
    for (const auto& pair : labelCounts) {
        if (pair.second > maxCount) {
            maxCount = pair.second;
            finalPredictedLabel = pair.first;
        }
    }

    // Check if the final predicted label has enough votes
    if (maxCount >= 3) {

        string name = "";
        auto it = all_users.find(finalPredictedLabel);
        if (it != all_users.end())
        {
            name = it->second;
        }
        cout << "********************" << endl;
        cout << "USER RECOGNIZED! | NAME : " << name << " ID: " << finalPredictedLabel << endl;
        return finalPredictedLabel;
        //cout << "Final predicted label: " << finalPredictedLabel << " (Votes: " << maxCount << ")" << endl;
    } else {
        cout << "> ERROR: User can not be recognized!" << endl;
        return -1;
    }
   
}

void deleteUser()
{
    int user_id = verifyUser();

    if (user_id >= 0)
    {
        cout << "> Deleting user ID : " << user_id << endl;

        auto it = all_users.find(user_id);
        if (it != all_users.end())
        {
            all_users.erase(it);
        }

        int size = all_faces.size();
        cout << "size : " << size << endl;

        for (int i = 0; i < size; i++)
        {
            if (all_labels[i] == user_id)
            {
                all_faces.erase(all_faces.begin() + i);
                all_labels.erase(all_labels.begin() + i);
            }
        }
        size = all_faces.size();
        cout << "size : " << size << endl;

        if (all_faces.size() == 0)
        {
            cout << "> There is no more users!" << endl;
            trained = false;
            model_update_status = false;
        }

        // delete from database
        database.deleteUser(user_id);

        model_update_status = false;
    }
    else
    {
        cout << "> ERROR: User does not exist in database!" << endl;
    }


}

void printMenu()
{
    cout << "---------------------------" << endl;
    cout << "---------------------------" << endl;
    cout << "Choose option:" << endl;
    cout << "1. add new user" << endl;
    cout << "2. verify user" << endl;
    cout << "3. delete user" << endl;
    cout << "4. exit" << endl;
    cout << "---------------------------" << endl;
}



void training()
{
    try 
    {
        model->train(all_faces, all_labels);
        cout << "> Training completed..." << endl;
        model_update_status = false;
        trained = true;
    } 
    catch (cv::Exception& e) 
    {
        cerr << "> Error during training: " << e.what() << endl;
        exit(23);
    }
}

int main() 
{
    // init model and load face detection model
    init();

    int input_value = 0;
   
    while (input_value != 4) {
        
        printMenu();
        readInput(&input_value);
        
        // happens only when new user is added or db is loaded
        if (model_update_status && ((input_value == 1) || (input_value == 2) || (input_value == 3)))
        {
          training();
        }

        switch (input_value) {
            case 1: 
            {
                printf("> Adding new user..\n");
                addNewUser();
                break;
            }
            case 2:
            {
                printf("> Verifying user..\n");
                verifyUser();
                break;
            }
            case 3:
                cout << "> Deleting user.." << endl;
                deleteUser();
                break;
            case 4:
                printf("> Exit..\n");
                break;
            default:
                printf("> Invalid option!\n");
                break;
        }
        
        

    }

    // Close the database connection
    //sqlite3_close(db);

    return 0;
}
