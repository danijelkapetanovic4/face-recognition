# Compiler
CC = g++

# Compiler flags
CFLAGS = -std=c++11 -Wall

# OpenCV flags
OPENCV_FLAGS = `pkg-config --cflags --libs opencv4`

# SQLite3 flags
SQLITE3_FLAGS = -lsqlite3

# Source files
SRCS = FaceRecognition.cpp DBHandler.cpp

# Executable name
EXE = FaceRecognition

all: $(EXE)

$(EXE): $(SRCS)
	$(CC) $(CFLAGS) $(SRCS) -o $(EXE) $(OPENCV_FLAGS) $(SQLITE3_FLAGS)

clean:
	rm -f $(EXE)
