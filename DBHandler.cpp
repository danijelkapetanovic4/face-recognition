#include "DBHandler.hpp"
#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;


DBHandler::DBHandler(bool *var,  map<int, string>& all_users, vector<cv::Mat>& all_faces, vector<int>& all_labels)
{
     // Check if the database file exists
    ifstream file("database.db");
    bool fileExists = file.good();
    file.close();

    // Open a database connection (create if not exists)
    int rc = sqlite3_open("database.db", &db);

    if (rc) {
        std::cerr << "> ERROR: Can't open database: " << std::endl;
        exit(25);
    } else {
        std::cout << "> Database opened successfully" << std::endl;
    }

    if (fileExists)
    {
        cout << "> Database already exist, loading data.." << endl;

        // SQL statement to select all columns from the USERS table
        const char* sql = "SELECT ID, Name FROM USERS;";

        // Prepare the SQL statement
        sqlite3_stmt* stmt;
        sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
       
        int key;
        string name;
        
        while (sqlite3_step(stmt) == SQLITE_ROW) 
        {
            key = sqlite3_column_int(stmt, 0); // Extract ID 
            const unsigned char* nameStr = sqlite3_column_text(stmt, 1); // Extract Name
            name = reinterpret_cast<const char*>(nameStr);

            all_users.insert(make_pair(key,name));
            cout << "User ID: " << key << "  | Name: " << name << endl;

            // SQL statement to retrieve faces for a specific user
            const char* sql = "SELECT Face FROM Faces WHERE UserID = ?;";

            sqlite3_stmt *stmt;
            sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
    
            // Bind the user ID to the prepared statement
            sqlite3_bind_int(stmt, 1, key);

            // Iterate over the results
            while (sqlite3_step(stmt) == SQLITE_ROW) 
            {
                // Retrieve the blob data for the face
                const void* blobData = sqlite3_column_blob(stmt, 0);
                int blobSize = sqlite3_column_bytes(stmt, 0);

                // Deserialize the blob data into a cv::Mat object
                cv::Mat face;
                vector<uchar> buffer(static_cast<const uchar*>(blobData), static_cast<const uchar*>(blobData) + blobSize);

                deserializeMat(buffer, face);
                all_faces.push_back(face);
                all_labels.push_back(key);
            }

            sqlite3_finalize(stmt);
        }

        sqlite3_finalize(stmt);
        if (all_faces.size() > 0)
            *var = true; // model_update_status is set to true -> train model

    }
    else
    {
        cout << "> Database does not exist, creating new.." << endl;

        // Create a table
        const char* createTableSQL =    "CREATE TABLE USERS(" \
                                        "ID INT PRIMARY KEY     NOT NULL," \
                                        "Name TEXT);";

        rc = sqlite3_exec(db, createTableSQL, 0, 0, 0);

        if (rc != SQLITE_OK) {
            std::cerr << "> ERROR: table users already created" << std::endl;
        } else {
            std::cout << "> Table Users created successfully" << std::endl;
        }

        // Create Faces table
        const char* createFacesTableSQL = "CREATE TABLE Faces ("
                                          "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                                          "UserID INTEGER,"
                                          "Face BLOB,"
                                          "FOREIGN KEY (UserID) REFERENCES Users(ID)"
                                          ");";

        rc = sqlite3_exec(db, createFacesTableSQL, nullptr, nullptr, 0);
        if (rc != SQLITE_OK) {
            std::cerr << "ERROR: table Faces already created" << std::endl;
        }else
        {
            std::cout << "Table Faces created successfully" << std::endl;
        }
    }
}


// Serialize a matrix to a byte vector
void serializeMat(const cv::Mat& matrix, vector<uchar>& buffer) {
    cv::imencode(".jpg", matrix, buffer);
}

// Decode the byte vector to a matrix
void deserializeMat(const vector<uchar>& buffer,cv:: Mat& matrix) {
    matrix = cv::imdecode(buffer, cv::IMREAD_GRAYSCALE);
}

void DBHandler::addUser(int key, std::string name, std::vector<cv::Mat> faces,    
                    std::vector<int> labels)
{
    cout << "- Adding new user to database..." << endl;

    // SQL statement with placeholders
    const char* sql = "INSERT INTO USERS (ID, Name) VALUES (?, ?);";

    sqlite3_stmt *stmt;
    int rc = sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);

    // Bind values to the placeholders in the prepared statement
    sqlite3_bind_int(stmt, 1, key);
    sqlite3_bind_text(stmt, 2, name.c_str(), -1, SQLITE_STATIC);
    // Execute the prepared statement
    rc = sqlite3_step(stmt);

    if (rc != SQLITE_DONE) {
        cerr << "SQL error: "  << endl;
    } else {
        cout << "User added successfully" << endl;
    }

    // Finalize the prepared statement
    sqlite3_finalize(stmt);

    for (size_t i = 0; i < faces.size(); i++) 
    {
        // Prepare the SQL INSERT statement
        std::stringstream ss;
        ss << "INSERT INTO Faces (UserID, Face) VALUES (?, ?);";
        std::string sql = ss.str();

        sqlite3_stmt *stmt;
        int rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
        if (rc != SQLITE_OK) {
            std::cerr << "Error preparing SQL statement: " << sqlite3_errmsg(db) << std::endl;
            return; // Exit the function if preparation fails
        }

        // Bind UserID to the prepared statement
        sqlite3_bind_int(stmt, 1, key);

        // Serialize the face into a buffer
        vector<uchar> buffer;
        serializeMat(faces[i], buffer);

        // Bind Face data to the prepared statement
        sqlite3_bind_blob(stmt, 2, buffer.data(), buffer.size(), SQLITE_STATIC);

        // Execute the SQL statement
        rc = sqlite3_step(stmt);
        /*if (rc != SQLITE_DONE) {
            std::cerr << "Error executing SQL statement: " << sqlite3_errmsg(db) << std::endl;
        } else {
            std::cout << "Face added successfully" << std::endl;
        }*/

        // Finalize the prepared statement
        sqlite3_finalize(stmt);
    }

}

void DBHandler::deleteUser(int key)
{

    cout << "> Deleting user from database.." << endl;

    std::string deleteFacesSQL = "DELETE FROM Faces WHERE UserID = " + std::to_string(key) + ";";

    sqlite3_exec(db, deleteFacesSQL.c_str(), nullptr, nullptr, 0);

    std::string deleteUsersSQL = "DELETE FROM Users WHERE ID = " + std::to_string(key) + ";";
    
    sqlite3_exec(db, deleteUsersSQL.c_str(), nullptr, nullptr, 0);

}